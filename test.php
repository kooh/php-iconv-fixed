<?php

$str = 'lökらっしゃいar';

echo "RAW:\n";
echo $str . "\n";

echo "ICONV_FIXED:\n";
var_dump( @iconv_fixed('UTF-8', 'CP437', $str) );

echo "ICONV_FIXED WITH //IGNORE\n";
var_dump( @iconv_fixed('UTF-8', 'CP437//IGNORE', $str) );
