#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "php.h"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include "iconv/php_iconv.h"

#include <iconv.h>


#define PHP_ICONV_FIXED_VERSION "1.0"
#define PHP_ICONV_FIXED_EXTNAME "iconv_fixed"

extern zend_module_entry iconv_fixed_module_entry;
#define phpext_iconv_fixed_ptr &iconv_fixed_module_entry


PHP_FUNCTION(iconv_fixed);

const zend_function_entry iconv_fixed_functions[] = {
    PHP_FE(iconv_fixed, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry iconv_fixed_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_ICONV_FIXED_EXTNAME,
    iconv_fixed_functions,
    NULL, // name of the MINIT function or NULL if not applicable
    NULL, // name of the MSHUTDOWN function or NULL if not applicable
    NULL, // name of the RINIT function or NULL if not applicable
    NULL, // name of the RSHUTDOWN function or NULL if not applicable
    NULL, // name of the MINFO function or NULL if not applicable
#if ZEND_MODULE_API_NO >= 20010901
    PHP_ICONV_FIXED_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

ZEND_GET_MODULE(iconv_fixed);


static void _php_iconv_show_error(php_iconv_err_t err, const char *out_charset, const char *in_charset TSRMLS_DC)
{
    switch (err) {
        case PHP_ICONV_ERR_SUCCESS:
            break;

        case PHP_ICONV_ERR_CONVERTER:
            php_error_docref(NULL TSRMLS_CC, E_NOTICE, "Cannot open converter");
            break;

        case PHP_ICONV_ERR_WRONG_CHARSET:
            php_error_docref(NULL TSRMLS_CC, E_NOTICE, "Wrong charset, conversion from `%s' to `%s' is not allowed",
                      in_charset, out_charset);
            break;

        case PHP_ICONV_ERR_ILLEGAL_CHAR:
            php_error_docref(NULL TSRMLS_CC, E_NOTICE, "Detected an incomplete multibyte character in input string");
            break;

        case PHP_ICONV_ERR_ILLEGAL_SEQ:
            php_error_docref(NULL TSRMLS_CC, E_NOTICE, "Detected an illegal character in input string");
            break;

        case PHP_ICONV_ERR_TOO_BIG:
            /* should not happen */
            php_error_docref(NULL TSRMLS_CC, E_WARNING, "Buffer length exceeded");
            break;

        case PHP_ICONV_ERR_MALFORMED:
            php_error_docref(NULL TSRMLS_CC, E_WARNING, "Malformed string");
            break;

        default:
            /* other error */
            php_error_docref(NULL TSRMLS_CC, E_NOTICE, "Unknown error (%d)", errno);
            break;
    }
}


PHP_FUNCTION(iconv_fixed)
{
    char *in_charset, *out_charset, *in_buffer, *out_buffer;
    size_t out_len;
    int in_charset_len = 0, out_charset_len = 0, in_buffer_len;
    php_iconv_err_t err;

    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sss",
        &in_charset, &in_charset_len, &out_charset, &out_charset_len, &in_buffer, &in_buffer_len) == FAILURE)
        return;

    if (in_charset_len >= ICONV_CSNMAXLEN || out_charset_len >= ICONV_CSNMAXLEN) {
        php_error_docref(NULL TSRMLS_CC, E_WARNING, "Charset parameter exceeds the maximum allowed length of %d characters", ICONV_CSNMAXLEN);
        RETURN_FALSE;
    }

    err = php_iconv_string(in_buffer, (size_t)in_buffer_len,
        &out_buffer, &out_len, out_charset, in_charset);
    _php_iconv_show_error(err, out_charset, in_charset TSRMLS_CC);
    if ((err == PHP_ICONV_ERR_SUCCESS || err == PHP_ICONV_ERR_ILLEGAL_SEQ && strstr(out_charset, "//IGNORE") != NULL) && out_buffer != NULL) {
        RETVAL_STRINGL(out_buffer, out_len, 0);
    } else {
        if (out_buffer != NULL) {
            efree(out_buffer);
        }
        RETURN_FALSE;
    }
}