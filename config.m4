PHP_ARG_ENABLE(iconv_fixed, enable iconv_fixed,
[ --enable-iconv_fixed   Enable iconv_fixed])
 
if test "$PHP_ICONV_FIXED" = "yes"; then
  AC_DEFINE(HAVE_ICONV_FIXED, 1, [Whether you have iconv_fixed])
  PHP_NEW_EXTENSION(iconv_fixed, iconv_fixed.c, $ext_shared)
fi
